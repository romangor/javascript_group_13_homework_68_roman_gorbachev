import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MessagesContainerComponent } from './messages-container/messages-container.component';
import { MessageDetailsComponent } from './messages-container/message-details/message-details.component';
import { MessageFormComponent } from './message-form/message-form.component';
import { MessageService } from './shared/message.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalComponent } from './ui/modal/modal.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesContainerComponent,
    MessageDetailsComponent,
    MessageFormComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }

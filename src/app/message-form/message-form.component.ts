import { Component, OnInit } from '@angular/core';
import { MessageModel } from '../shared/message.model';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {
  author: string = '';
  authorName!: string;
  text!: string;
  isModalOpen = false;
  loading!: boolean;

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
    this.messageService.sendingMessage.subscribe((isSend: boolean) => {
      this.loading = isSend;
    })
  }

  sendMessage() {
    if(this.author === ''){
       alert('Enter author name');
    } else {
      const datetime = new Date().toString();
      const id = Math.random().toString();
      const message = new MessageModel(this.text, this.author, datetime, id);
      this.messageService.sendMessage(message);
      this.text = '';
    }

  }

  changeAuthor() {
    this.author = this.authorName
  }

  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }
}

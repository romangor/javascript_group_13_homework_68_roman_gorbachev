import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessageModel } from '../shared/message.model';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-messages-container',
  templateUrl: './messages-container.component.html',
  styleUrls: ['./messages-container.component.css']
})
export class MessagesContainerComponent implements OnInit, OnDestroy {
  messages: MessageModel[] = [];
  loading: boolean = true;

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
    this.messageService.start();
    this.messageService.messagesChange.subscribe((messages: MessageModel[]) => {
      this.messages = messages;
    });
    this.messageService.fetchingMessages.subscribe(isLoading => {
      this.loading = isLoading;
    })
  }

  ngOnDestroy() {
    this.messageService.stop();
  }

}

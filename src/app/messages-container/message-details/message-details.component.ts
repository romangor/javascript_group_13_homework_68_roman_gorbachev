import { Component, Input } from '@angular/core';
import { MessageModel } from '../../shared/message.model';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.css']
})
export class MessageDetailsComponent {
  @Input() message!: MessageModel;}

import { HttpClient, HttpParams } from '@angular/common/http';
import { MessageModel } from './message.model';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { interval, Subject, Subscription } from 'rxjs';

@Injectable()
export class MessageService {
  messagesChange = new Subject<MessageModel[]>();
  fetchingMessages = new Subject<boolean>();
  sendingMessage = new Subject<boolean>();
  messages: MessageModel[] = [];
  subscription!: Subscription;

  constructor(private http: HttpClient) {}

  fetchMessages() {
    this.fetchingMessages.next(true);
    this.http.get<{ [id: string]: MessageModel }>('http://146.185.154.90:8000/messages?datetime=2021-12-21T10:20:39.586Z')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new MessageModel(data.message, data.author, data.datetime, data.id)
        })
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice().reverse());
        this.fetchingMessages.next(false);
      })
  }

  start() {
    this.subscription = interval(3000).subscribe(() => {
      this.fetchMessages();
    });
  }

  stop() {
    this.subscription.unsubscribe();
  }

  sendMessage(message: MessageModel) {
    this.sendingMessage.next(true);
    const body = new HttpParams()
      .set('author', `${message.author}`)
      .set('message', `${message.message}`);
    this.http.post('http://146.185.154.90:8000/messages', body)
      .pipe(tap(() => {
        this.sendingMessage.next(false);
      }, () => {
        this.sendingMessage.next(false);
      }))
      .subscribe(() => {
      this.sendingMessage.next(false);
    });
  }
}

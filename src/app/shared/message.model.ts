export class MessageModel {
  constructor(public message: string,
              public author: string,
              public datetime: string,
              public id: string) {}
}
